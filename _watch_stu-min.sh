#!/bin/bash

min_disk_space_gb=1
/usr/openv/netbackup/bin/admincmd/bpstulist | while read -a stu -r; do
	if [ ${stu[1]} -eq 0 ]; then
		read -a stuext <<< $(/usr/openv/netbackup/bin/admincmd/nbdevquery -listdv -l -stype BasicDisk -dp ${stu[0]})
		if [ ${stuext[6]%%[.,]*} -le $min_disk_space_gb -a ${stu[6]} != "0" ]; then
			/usr/openv/netbackup/bin/admincmd/bpsturep -label ${stu[0]} -cj 0
		fi
	fi
done
