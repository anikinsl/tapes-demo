#!/bin/sh

### Logging
###
script_name=`basename $0`
logfile=`dirname $0`/log/$script_name.log
[ -d `dirname $0`/log ] || mkdir -p `dirname $0`/log
logger () {
	logline () {
		if [ "$2" ]; then
			indent="	"
		fi
		TIMESTAMP=`LANG=C date '+%b %e %T'`
		printf "$TIMESTAMP %s[$$]:%s %s\n" "$script_name" "$indent" "$1" >>$logfile
	}

	if [ -z "$1" ]; then
		cat - | while read line; do
			logline "$line" inline
		done
	else
		logline "$1"
	fi
}
#####

# Фактические номера роботов TLD.
#robots="0 1 2 3 4"
robots=`/usr/openv/volmgr/bin/tpconfig -d | grep 'robotic path' | sed -e 's,^.*TLD(\([0-9]\).*$,\1,'`
logger "Using robots: $robots"

for robot in $robots; do
	case $robot in
	0)	robot_media_type=hcart3; robot_volgrp=000_00000_TLD
		;;
	1)	robot_media_type=hcart;  robot_volgrp=000_00001_TLD
		;;
	2)	robot_media_type=hcart2; robot_volgrp=000_00002_TLD
		;;
	3)	robot_media_type=hcart2; robot_volgrp=000_00003_TLD
		;;
	4)	robot_media_type=hcart2; robot_volgrp=000_00004_TLD
		;;
	esac

	logger "[$robot] Default media type for robot is '$robot_media_type' and volume group is '$robot_volgrp'"

	### Формат вывода команды "vmquery -rn 1 -w":
	### 0855L4  -       HCART     DY0855L4          -                 xe0455        TLD        1       1  -      000_00001_TLD              NetBackup                  1  Scratch_pool              12       0          -  11/02/2011 13:54  08/29/2012 01:00  11/04/2011 01:37  08/29/2012 01:08  00/00/0000 00:00       0  -                          00/00/0000 00:00  00/00/0000 00:00  -       -             50  Added by Media Manager   

	# Запрос всех лент на роботе
	/usr/openv/volmgr/bin/vmquery -rn $robot -w \
	| sed -e '1,/^----/d' \
	| awk '{ print $1,tolower($3),$11,$7,$6,$9 }' \
	| while read media_id media_type volgrp robot_type robot_host media_slot; do
		if [ $media_type = $robot_media_type -a $volgrp = $robot_volgrp ]; then
			continue
		fi

		logger "[$robot] $media_id has media type '$media_type' and volume group '$volgrp'."

		# Пропуск чистящих лент
		case $media_type in
		hc*cln|HC*CLN)	logger "[$robot] It is cleaning tape. Skipping."
			continue
			;;
		esac

		# Ничего не менять, просто вывести команды. Эти команды потом можно
		# передавать в sh через pipe: 'fix_media_volgrp.sh | sh'.
		logger "[$robot] Building shell commands (Nothing execute actually):"
		logger "[$robot]   vmchange -new_mt $robot_media_type -m $media_id"
		logger "[$robot]   vmchange -res -m $media_id -mt $robot_media_type -rt $robot_type -rn $robot -rh $robot_host -rc1 $media_slot -v $robot_volgrp"

		echo vmchange -new_mt $robot_media_type -m $media_id
		echo vmchange -res -m $media_id -mt $robot_media_type -rt $robot_type -rn $robot -rh $robot_host -rc1 $media_slot -v $robot_volgrp
	done
done
logger "All done."
