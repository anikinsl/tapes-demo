#!/bin/sh

# Пример входных данных:
#
# 28.02.2013 12:26:00 - media OL8940 required
# 28.02.2013 12:26:00 - media OL8696 required
#
# а также:
#
# 28.02.2013 12:26:00 - media required OL8940
#
# или:
#
# OL8940

T=/tmp/whereis_tape_summary

exact_match=
for opt; do
	case $opt in
	--exact)	exact_match=-w
		;;
	esac
done

/usr/openv/volmgr/bin/vmquery -a -w >$T
cat - | while read line; do
	#id=`echo "$line" | grep -o '[0-9A-Z]\{4\}L[3-9]'`
	id=`echo "$line" | grep -o '[0-9A-Z]\{6\}'`
	cat $T | grep $exact_match "$id" | awk '{ print $1" "$3" "$7" "$8" "$9 }'
done | sort -k3
