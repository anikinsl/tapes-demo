#!/bin/sh

# --------------------------------------------------------------------
# TODO: Этот скрипт имеет зависимость со скриптом list_expired_media.sh, который
# должен находиться в одной директории с этим скриптом.
# --------------------------------------------------------------------

script_name=`basename $0`
tmp=`mktemp -u /tmp/lt-XXXXX`
lastvmquery_file=`dirname $0`/auto_list_expired_lastquery.txt

# --------------------------------------------------------------------
# Logging
# --------------------------------------------------------------------

logfile=`dirname $0`/log/$script_name.log
[ -d `dirname $0`/log ] || mkdir -p `dirname $0`/log
logger () {
	logline () {
		if [ "$2" ]; then
			indent="	"
		fi
		TIMESTAMP=`LANG=C date '+%b %e %T'`
		printf "$TIMESTAMP %s[$$]:%s %s\n" "$script_name" "$indent" "$1" >>$logfile
	}

	if [ -z "$1" ]; then
		cat - | while read line; do
			logline "$line" inline
		done
	else
		logline "$1"
	fi
}

# --------------------------------------------------------------------
# Единая точка выхода с возможностью вывода номера ошибки.
# --------------------------------------------------------------------

quit () {
	# Безопасное удаление
	for t in ${tmp}-*; do
		if echo $t | grep '/tmp/lt-[a-zA-Z0-9]*-[a-zA-Z0-9]*' >/dev/null; then
			rm -f $t
		fi
	done
	rm -f $pidfile
	exit $1
}

pidfile=/var/run/auto_list_expired.pid
if [ -f $pidfile ]; then
	if ps -o pid= `cat $pidfile` >/dev/null; then
		logger "Already running an instance. Check a locking file: $pidfile"
		# Don't use 'quit' function
		exit 2
	fi
fi
echo $$ >$pidfile

# --------------------------------------------------------------------
# Сбор данных о текущем состоянии лент. Нас будет интересовать только
# идентификатор ленты и его расположение: в библиотеке или вне библиотеки.
# --------------------------------------------------------------------

/usr/openv/volmgr/bin/vmquery -a -w | sed -e '1,/^----/d' >${tmp}-curvmquery

if ! [ -f ${lastvmquery_file} ]; then
	logger "Last vmquery results are unavailable. Creating initial data file ${lastvmquery_file}"
	cp ${tmp}-curvmquery ${lastvmquery_file}
	quit 0
fi

cat ${tmp}-curvmquery | awk '{ if ($7 == "NONE") print $LINE }' >${tmp}-standalone
cat ${lastvmquery_file} | awk '{ if ($7 != "NONE") print $LINE }' >${tmp}-inrobot

[ `cat ${tmp}-standalone | wc -l` -eq 0 ] && (
	logger "${tmp}-standalone is empty. Exit."
	quit 1
)

[ `cat ${tmp}-inrobot | wc -l` -eq 0 ] && (
	logger "${tmp}-inrobot is empty. Exit."
	quit 1
)

touch ${tmp}-moved

# --------------------------------------------------------------------
# Анализ расположения ленты.
# Нам нужны только те ленты, которые сейчас находятся вне какой-либо из
# библиотек, но в прошлый раз находившихся там.
# --------------------------------------------------------------------

cat ${tmp}-inrobot | while read line
do
	media_id=`echo "$line" | awk '{ print $4 }'`
	if grep -- "$media_id" ${tmp}-standalone >/dev/null; then
		echo $media_id moved to standalone.
		echo "$line" >>${tmp}-moved
	fi
done

# --------------------------------------------------------------------
# Проверка правильности volume group на всех лентах.
# --------------------------------------------------------------------

touch ${tmp}-vg
sh `dirname $0`/fix_media_volgrp.sh >${tmp}-vg
vgfile_count=`cat ${tmp}-vg | wc -l`
vgfile_count=`echo ${vgfile_count} / 2 | bc`
if [ ${vgfile_count} -gt 0 ]; then
	echo "" >>${tmp}-vg
	echo "Count: ${vgfile_count}" >>${tmp}-vg

	cat ${tmp}-vg | mail -s "[fix_media_volgrp] *** Need to fix volume groups ***" vyacheslav.anikin@deleteddomain.ru
fi

# --------------------------------------------------------------------
# Выход, если расположение лент не изменилось
# --------------------------------------------------------------------

if [ `cat ${tmp}-moved | wc -l` -le 0 ]; then
	logger "Nothing changed."
	echo "Ничего не изменилось."
	quit
fi

# --------------------------------------------------------------------
# NB: Вызов внешнего скрипта, который формирует письма для
# рассылки с указанием дат окончания срока хранения лент.
# --------------------------------------------------------------------

if [ -f `dirname $0`/list_expired_media.sh ]; then
	logger "Running external script: `dirname $0`/list_expired_media.sh"
	sh `dirname $0`/list_expired_media.sh --format=vmquery -m ${tmp}-moved
	logger "End of external script."
else
	echo "Couldn't find `dirname $0`/list_expired_media.sh"
	quit
fi

# --------------------------------------------------------------------
# Фиксация текущего состояния лент для следующего запуска этого скрипта.
# --------------------------------------------------------------------

cp ${tmp}-curvmquery ${lastvmquery_file}

quit 0
